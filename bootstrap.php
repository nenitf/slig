<?php

require "vendor/autoload.php";

use Slim\App;

/*
* Codigo em produção
*/
//$app = new App;


/*
* Codigo em desenvolvimento
*/

$config['displayErrorDetails'] = true;

$app = new App(['settings' => $config]);
