<?php

// die dump
function dd($data){
  echo "<pre>";
  var_dump($data);
  echo "</pre>";

  die();
}

function json($data){
  header('Content-Type: application/json');

  echo json_encode($data);
}

// retorna à raiz do projeto, exemplo: "/home/usuario/projeto-twig-slim"
function path(){
  $vendorDir = dirname(dirname(__FILE__));
  return dirname($vendorDir);
}

// upload de arquivos pelo $arrayFiles = array('file' => $_FILES['aerodromosPrivados'], 'name' => 'aerodromos privados.csv')
function trataUploads($arrayFiles)
{
  $pathUploads = path().'/app/uploads';

  if (!is_dir($pathUploads))
  {
    mkdir($pathUploads);
  }

  foreach ($arrayFiles as $file)
  {
    if ($file['file']['error'])
    {
      throw new Exception("ERRO EM UM DOS 3 ARQUIVOS");
    }

    if (move_uploaded_file($file['file']['tmp_name'], $pathUploads . DIRECTORY_SEPARATOR . $file['name']))
    {
      // echo 'tudo certo!';
    }
    else
    {
      throw new Exception("Não foi possível fazer o upload");
    }
  }
}
