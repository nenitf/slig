<?php

namespace app\controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends Controller
{
  public function index()
  {
    // chamar twig pela trait
    $this->view('home', [
      'nome' => 'Alexandre',
      'title' => 'Home'
    ]);

    // $this->view('nome-do-html', [
    //   'variavel' => 'valor'
    // ]);

    // diretorios dentro dos templates divididos com ponto .
    // $this->view('pasta.pasta.nome-do-html', [
    //   'variavel' => 'valor'
    // ]);
  }

  public function show(Request $request, Response $response, array $args)
  {
    $this->view('home', [
      'nome' => $args['nome'],
      'title' => 'Show'
    ]);
  }
}
